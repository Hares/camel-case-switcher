from unittest import TestCase

class BaseTestCase(TestCase):
    def assertEqual(self, actual, expected, msg = None):
        """ PyCharm swaps expected & actual compared to the use case in this project. """
        super().assertEqual(expected, actual, msg=msg)


__all__ = \
[
    'BaseTestCase',
]
